$(document).ready(() => {
  const userCookie = getCookie('user');

  var headers = {
    Authorization: 'Bearer ' + userCookie,
  };

  var urlInfo = 'http://42.115.221.44:8080/devcamp-auth/users/me';

  $.ajax({
    url: urlInfo,
    method: 'GET',
    headers: headers,
    success: function (responseObject) {
      displayUser(responseObject);
    },
    error: function (xhr) {
      console.log(xhr);
      // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
      redirectToLogin();
    },
  });

  function displayUser(paramUser) {
    console.log(paramUser);
    $('#input-first-name').val(paramUser.firstname);
    $('#input-last-name').val(paramUser.lastname);
    $('#input-email').val(paramUser.email);
  }

  function redirectToLogin() {
    // Trước khi logout cần xóa token đã lưu trong cookie
    setCookie('user', '', 1);
    window.location.href = 'signIn.html';
  }

  $('#btn-log-out').click(redirectToLogin);

  function getCookie(cname) {
    let name = cname + '=';
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  }
  function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
  }
});
